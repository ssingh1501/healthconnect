import mosquitto
import time
#import pyserial

BROKER_ADDRESS = 'broker.mqttdashboard.com'
BROKER_PORT = 1883
FILE_NAME = '/root/sensor1.txt'

client = mosquitto.Mosquitto('test-clinet-id')
retval = client.connect(BROKER_ADDRESS,BROKER_PORT,keepalive=60)
print (retval)

#blueSerial = serial.Serial("/dev/rfcomm1",baudrate = 9600, timeout = 1)


sensorvalue = []
sensorValue1 = []
#f = open(FILE_NAME,'r+')
#sensorvalue = f.readline()

def on_connect(mosq, obj, rc):
      if rc == 0:
                print("Connected successfully.")
      elif rc == 1:
                print("unacceptable protocol version")
      elif rc == 2:
                print("identifier rejected")
      elif rc == 3:
                print("server unavailable")
      elif rc == 4:
                print("Bad user name or password")
      elif rc == 5: 
                print("Not authorized")


def on_publish(mosq, obj, mid):
      print("Message "+str(mid)+" published.")


client.on_connect = on_connect
client.on_publish = on_publish

while 1:
  #blueSerial.write("$Read Sensor@")
  #sensorvalue = blueSerial.read(100)

  f = open(FILE_NAME,'r+')
  sensorvalue = f.readline()
  print(len(sensorvalue))
  if (len(sensorvalue) == 14):
    client.publish("Autoentity/Weight",sensorvalue[9:13])
  elif (len(sensorvalue) == 13):
    client.publish("Autoentity/Weight",sensorvalue[9:12])
  print(sensorvalue[9:13])
  f.close()
  client.loop()
  time.sleep(1)
