import socket
import ssl

s_ = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s = ssl.wrap_socket(s_, ca_certs='/usr/local/lib/python2.7/dist-packages/requests/cacert.pem',cert_reqs=ssl.CERT_REQUIRED)
s.connect(('www.google.com', 443))
s.write("""GET / HTTP/1.1\r
Host: www.google.com\r\n\r\n""")
d=s.read()
print(d)
s.close()
