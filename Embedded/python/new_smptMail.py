# coding: utf-8
from email.mime.text import MIMEText
from datetime import date
import smtplib
SMTP_SERVER = 'smtp.gmail.com'
SMTP_PORT = 587
SMTP_USERNAME = 'shailendra@autoentity.com'
SMTP_PASSWORD = '12Qwaszx12'

EMAIL_TO = ["srj0408@gmail.com","keshava1501@gmail.com","mayank.knit0407@gmail.com"]
EMAIL_FROM = 'shailendra@autoentity.com'
EMAIL_SUBJECT = 'Autoentity weight notifications: '

DATE_FORMAT = '%d/%m/%Y'
EMAIL_SPACE = ', '

DATA= 'Weight Notifications\r\nAnalysis Over a week:\r\nMaximum Weight : 67 K.g.\r\nMinimum Weight : 65 K.g.\r\nAverage Weight : 66.2 K.g.\r\nSuggestions : Start Dieting'

def send_email():
    msg = MIMEText(DATA)
    msg['Subject'] = EMAIL_SUBJECT + ' %s' % (date.today().strftime(DATE_FORMAT))
    msg['To'] = EMAIL_SPACE.join(EMAIL_TO)
    msg['From'] = EMAIL_FROM
    mail = smtplib.SMTP(SMTP_SERVER, SMTP_PORT)
    mail.starttls()
    mail.login(SMTP_USERNAME, SMTP_PASSWORD)
    mail.sendmail(EMAIL_FROM, EMAIL_TO, msg.as_string())
    mail.quit()
    print 'Email Send'

if __name__=='__main__':
    send_email()
