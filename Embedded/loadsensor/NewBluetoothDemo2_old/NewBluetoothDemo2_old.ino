/*SilverLine Design Automation Pvt Ltd.*/
/*Code for 2 way communication betwen Bluetooth Android app and ARduino + Bluetooth Module*/
/*For switch debouncing, currently i am using delay, but the better way is to use Interrupt.*/


#include <SoftwareSerial.h>

SoftwareSerial BluetoothSerial(9,10); // RX | TX
const int buttonPin = 2;     
const int ledPin =  13;      
int sensorPin = A0; 
int sensorValue = 0;

int buttonState = 0;        
int count = 0;
void setup() {
 
  pinMode(ledPin, OUTPUT);      
  pinMode(buttonPin, INPUT);   
  pinMode(buttonPin, INPUT); 
  Serial.begin(9600);
  Serial.println("Enter AT commands:");
  BluetoothSerial.begin(9600); 
  buttonState = digitalRead(buttonPin);
  digitalWrite(ledPin,LOW);  
}

void loop(){
  if (BluetoothSerial.available()){
     Serial.write(BluetoothSerial.read() );
         
   }

  
     sensorValue = analogRead(sensorPin);
      
    BluetoothSerial.print("Player Sword touches oponent ");
    BluetoothSerial.print(sensorValue);
    BluetoothSerial.println("times");
   
  
}

