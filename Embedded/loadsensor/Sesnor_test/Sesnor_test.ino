
#include <LiquidCrystal.h>
LiquidCrystal lcd(12, 11, 5, 4, 3, 2);
int sensorPin = A0; 
int sensorValue = 0;
#include <stdio.h>
#include <SoftwareSerial.h>

void setup()
{
  // set up the LCD's number of columns and rows: 
  lcd.begin(16, 2);
  // Print a message to the LCD.
  lcd.print("Autoentity Technologies");
  delay(3000);
  
  Serial.begin(9600);
  
  
  }
 
 void loop(){
 
  sensorValue = analogRead(sensorPin);
  lcd.setCursor(0, 1);
  lcd.print("Weight=");
  
  lcd.setCursor(9,1);
  lcd.print(sensorValue);
  delay(1000);
  //lcd.clear();
}

