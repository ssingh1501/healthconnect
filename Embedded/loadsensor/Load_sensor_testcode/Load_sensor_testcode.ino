#include<stdio.h>
#include <LiquidCrystal.h>
LiquidCrystal lcd(12, 11, 5, 4, 3, 2);

int sensorValue = 0;
int sensorPin = A0;


void setup()
{

  Serial.begin(9600);
  Serial.println("Weight sensor reading");
  lcd.begin(16, 2);
  
}

void loop()
{
  lcd.begin(16, 2);
  lcd.setCursor(0,0);
  lcd.print("Weight measurement");
  sensorValue = analogRead(sensorPin);
  Serial.println(sensorValue);
  lcd.setCursor(0,1);
  lcd.print(sensorValue);
  delay(200);
}
